Angular project for playing and experimenting with various plugins etc.

* Uses node http-server for serving static files from public folder
  * Started by npm start
  * Site accessed at http://localhost:9000
* Web dependencies managed by bower
  * Auto executed by script on npm install
  * .bowerrc controls location of downloaded assets
